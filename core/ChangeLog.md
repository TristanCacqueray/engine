# Changelog for keid-core

## 0.1.7.1

- SPIR-V reflection loader changed to `spirv-reflect-ffi`.

## 0.1.7.0

- Graphics pipelines extracted to `Engine.Vulkan.Pipeline.Graphics`.
- Added pipeline stage collections and `StageInfo` class.
  * Shader creation can now pluck stage flags from the collection.
- Added `Engine.Vulkan.Pipeline.Raytrace` stages (no pipelines yet).
- Added `Engine.Vulkan.Pipeline.External` worker for reloading pipelines.
- Added `Engine.SpirV.Reflect` to facilitate safe reloads using `spirv-reflect` toolchain.
- Added `Engine.Engine.Stage.Component` with rendering/resources/scenes decoupled.

## 0.1.6.1

- Minimal `vulkan` version is now 3.17.
- Fixed VMA setup for headless rendering.
- Add `Generically1` from `base-4.17` to replace `distributive`/`repesentable`.
- `Engine.Worker.Cell` instances generalized to arbitrary pairs.
  * I.e. `(Var i, Var o)` is a `Cell`-like structure that can be driven externally
    while retaining the `Var` and `Observer` machinery.
- `withFrozenCallStack` fixes for GHC-9.0.
- `Engine.Worker` boilerplate for coherent `Resource.Buffer`.
- Added `Resource.Image.Atlas` experimental container for tile atlases.
- Added `Resource.Region` for detachable allocation scopes.
- Added `Engine.Window.MouseButton.Collection` to facilitate button-related dispatch.
- Added a shortcut around loop when a window is quitting.
  * Skips allocation of loop resources that wouldn't be used.
  * Sidesteps a race before release/allocate chain when unwinding stages leading to crashes.
- Added `Engine.UI.Layout.Linear`.
  * Experimental for now, but require no workers and FRP-compatible.

## 0.1.6.0

- Added `Engine.Camera` with projection and view controls.
- Worker `Cell` changed into an input-annotated `Merge`.

## 0.1.5.1

- Fixed `VMA.AllocatorCreateInfo` for `vulkan-3.15` and newer.

## 0.1.5.0

- Removed `allocateRenderpass_` from `RenderPass` class.
- Added `allocateRP` stage field.
- Added `Engine.Vulkan.Pipeline.Compute`.
  * Common shader code extracted to `Engine.Vulkan.Shader`.
- Added shader specialization constants.
  * Graphic and compute pipeline configs extended with `spec` parameter.
  * `Shader.Specialization` class is used to derive vulkan schema at runtime and store values.
- Added `Pipline.Configure` and `Compute.Configure` families to produce a matching `Config` type.
- Added `cDepthCompare` to `Pipeline.Config`.
- Removed `Zero` instance for pipline configs.
  * Use `baseConfig` value for better type inference and less imports.

## 0.1.4.1

- Added size information to `Resource.Image`: `aiExtent`.

## 0.1.4.0

- Changed `Render.Samplers.allocate` to use `maxAnisotropy` directly.
- Relaxed `DescriptorSet.allocatePool` to any `MonadResource`.
- Added KHR_Surface to headless requirements.
- Added `Render.Pass.setViewportScissor`.
- Added TRANSFER_SRC to Offscreen image usage.
- Published `Render.Samplers.params`.

## 0.1.3.1

- Added `Engine.Setup.setupHeadless` to use without a surface.
- Added queue assignment fallback when there are no distinct queues for compute and transfer.
  Fixes startup crash on Intel iGPUs.

## 0.1.3.0

- Changed mesh codec to use `Serialise` class instead of `Storable`.
- Replaced RIO in resource loaders with constraints.
- Added `Render.Pass` base module and `Render.Pass.Offscreen`.
- `Resource.Image.AllocatedImage` got format and subresource information.
- Removed `Resource.Image.createColorResource`, `Resource.Image.createDepthResource`.
  * Added unified `Resource.Image.create` instead.
  * Function names shortened to match qulified import style.

## 0.1.2.0

- Added `Resource.Source` type to mix external and embedded byte sources.
- Added `Zstd.Compressed` wrapper and functions to compress/decompress `ByteString`.
- Removed `Ktx1.createTexture`.
  - Added `load`, `loadBytes`, `loadKtx1` instead.

## 0.1.1.1

- Added `spawnMergeT` to merge traversable collections.
- Added `HasStateRef` to make stage state accessible from `StageFrameRIO`.
- Added module labels to pipelines and their layouts.

## 0.1.1.0

- Added `--display NUM` to application arguments.
- Allow `Timed` workers to skip output update.
- `spawnTimed` and `spawnTimed_` changed args: active/dt moved to front.
- `spawnTimed` now uses initialization function instead of deriving initial output from initial state.

## 0.1.0.1

- Inter-stage resource release waits for render job to finish.
