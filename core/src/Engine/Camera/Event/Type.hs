module Engine.Camera.Event.Type
  ( Event(..)
  ) where

import RIO

import Geomancy (Vec3)

data Event
  = Zoom Float
  | Pan Vec3
  | PanHorizontal Float
  | PanVertical Float
  | TurnAzimuth Float
  | TurnInclination Float
  deriving (Show)
