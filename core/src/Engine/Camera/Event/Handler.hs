module Engine.Camera.Event.Handler
  ( handler
  ) where

import RIO

import Engine.Camera qualified as Camera
import Engine.Camera.Controls qualified as Controls
import Engine.Camera.Event.Type (Event)
import Engine.Camera.Event.Type qualified as Event
import Engine.Worker qualified as Worker

handler
  :: MonadIO m
  => m Controls.ViewProcess
  -> m Controls.ControlsProcess
  -> Event
  -> m ()
handler getViewP getCameraControls = \case
  Event.Zoom delta -> do
    cameraView <- getViewP
    Worker.pushInput cameraView \voi -> voi
      { Camera.orbitDistance =
          max 0.5 $ Camera.orbitDistance voi - delta * 0.5
      }

  Event.Pan delta -> do
    cameraView <- getViewP
    Controls.panInstant cameraView delta

  Event.PanHorizontal delta -> do
    controls <- getCameraControls
    Worker.modifyConfig (Controls.panHorizontal controls) $
      const delta

  Event.PanVertical delta -> do
    controls <- getCameraControls
    Worker.modifyConfig (Controls.panVertical controls) $
      const delta

  Event.TurnAzimuth delta -> do
    controls <- getCameraControls
    Worker.modifyConfig (Controls.turnAzimuth controls) $
      const delta

  Event.TurnInclination delta -> do
    controls <- getCameraControls
    Worker.modifyConfig (Controls.turnInclination controls) $
      const delta
