module Engine.Window.CursorPos
  ( Callback
  , callback

  , GLFW.MouseButton(..)
  , GLFW.MouseButtonState(..)
  , GLFW.ModifierKeys(..)

  , mkCallback
  ) where

import RIO

import Graphics.UI.GLFW qualified as GLFW
import RIO.App (appEnv)
import UnliftIO.Resource (ReleaseKey)
import UnliftIO.Resource qualified as Resource

import Engine.Types (GlobalHandles(..), StageRIO)

type Callback st = Double -> Double -> StageRIO st ()

callback :: Callback st -> StageRIO st ReleaseKey
callback handler = do
  window <- asks $ ghWindow . appEnv
  withUnliftIO \ul ->
    GLFW.setCursorPosCallback window . Just $ mkCallback ul handler
  Resource.register $
    GLFW.setCursorPosCallback window Nothing

mkCallback :: UnliftIO (StageRIO st) -> Callback st -> GLFW.CursorPosCallback
mkCallback (UnliftIO ul) action =
  \_window px py ->
    ul $ action px py
