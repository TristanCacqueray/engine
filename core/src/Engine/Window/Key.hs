module Engine.Window.Key
  ( Callback
  , callback

  , GLFW.Key(..)
  , GLFW.KeyState(..)
  , GLFW.ModifierKeys(..)

  , mkCallback
  ) where

import RIO

import Graphics.UI.GLFW qualified as GLFW
import RIO.App (appEnv)
import UnliftIO.Resource (ReleaseKey)
import UnliftIO.Resource qualified as Resource

import Engine.Types (GlobalHandles(..), StageRIO)

type Callback st = Int -> (GLFW.ModifierKeys, GLFW.KeyState, GLFW.Key) -> StageRIO st ()

callback :: Callback st -> StageRIO st ReleaseKey
callback handler = do
  window <- asks $ ghWindow . appEnv
  withUnliftIO \ul ->
    GLFW.setKeyCallback window . Just $ mkCallback ul handler
  Resource.register $
    GLFW.setKeyCallback window Nothing

mkCallback :: UnliftIO (StageRIO st) -> Callback st -> GLFW.KeyCallback
mkCallback (UnliftIO ul) action =
  \_window key keyCode keyState mods ->
    ul $ action keyCode (mods, keyState, key)
