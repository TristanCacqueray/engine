module Engine.Window.Scroll
  ( Callback
  , callback

  , mkCallback
  ) where

import RIO

import Graphics.UI.GLFW qualified as GLFW
import RIO.App (appEnv)
import UnliftIO.Resource (ReleaseKey)
import UnliftIO.Resource qualified as Resource

import Engine.Types (GlobalHandles(..), StageRIO)

type Callback st = Double -> Double -> StageRIO st ()

callback :: Callback st -> StageRIO st ReleaseKey
callback handler = do
  window <- asks $ ghWindow . appEnv
  withUnliftIO \ul ->
    GLFW.setScrollCallback window . Just $ mkCallback ul handler
  Resource.register $
    GLFW.setScrollCallback window Nothing

mkCallback :: UnliftIO (StageRIO st) -> Callback st -> GLFW.ScrollCallback
mkCallback (UnliftIO ul) action =
  \_window dx dy ->
    ul $ action dx dy
