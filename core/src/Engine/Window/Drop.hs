module Engine.Window.Drop
  ( Callback
  , callback

  , mkCallback
  ) where

import RIO

import Graphics.UI.GLFW qualified as GLFW
import RIO.App (appEnv)
import UnliftIO.Resource (ReleaseKey)
import UnliftIO.Resource qualified as Resource

import Engine.Types (GlobalHandles(..), StageRIO)

type Callback st = [FilePath] -> StageRIO st ()

callback :: Callback st -> StageRIO st ReleaseKey
callback handler = do
  window <- asks $ ghWindow . appEnv
  withUnliftIO \ul ->
    GLFW.setDropCallback window . Just $ mkCallback ul handler
  Resource.register $
    GLFW.setDropCallback window Nothing

mkCallback :: UnliftIO (StageRIO st) -> Callback st -> GLFW.DropCallback
mkCallback (UnliftIO ul) action =
  \_window files ->
    ul $ action files
