module Engine.Events.MouseButton
  ( ClickHandler
  , callback
  , handler
  ) where

import RIO

import Geomancy (Vec2)
import UnliftIO.Resource (ReleaseKey)

import Engine.Events.Sink (Sink)
import Engine.Types (StageRIO)
import Engine.Window.MouseButton (ModifierKeys, MouseButton, MouseButtonState)
import Engine.Window.MouseButton qualified as MouseButton
import Engine.Worker qualified as Worker

type ClickHandler e st =
  Sink e st ->
  Vec2 ->
  (ModifierKeys, MouseButtonState, MouseButton) ->
  StageRIO st ()

callback
  :: ( Worker.HasOutput cursor
     , Worker.GetOutput cursor ~ Vec2
     )
  => cursor
  -> ClickHandler e st
  -------------------------
  -> Sink e st
  -> StageRIO st ReleaseKey
callback cursorP eventHandler = MouseButton.callback . handler cursorP eventHandler

handler
  :: ( Worker.HasOutput cursor
     , Worker.GetOutput cursor ~ Vec2
     )
  => cursor
  -> ClickHandler e st
  -> Sink e st
  -> (ModifierKeys, MouseButtonState, MouseButton)
  -> StageRIO st ()
handler cursorP eventHandler sink buttonEvent = do
  cursorPos <- Worker.getOutputData cursorP
  logDebug $ "MouseButton event: " <> displayShow (cursorPos, buttonEvent)
  eventHandler sink cursorPos buttonEvent
