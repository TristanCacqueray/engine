module Engine.App
  ( engineMain
  , engineMainWith
  ) where

import RIO

import RIO.App (appMain)
import Engine.Run (runStack)
import Engine.Setup (setup)
import Engine.Types qualified as Engine
import Engine.Types.Options (Options(..), getOptions)
import Engine.Stage.Bootstrap.Setup qualified as Bootstrap

engineMain :: Engine.StackStage -> IO ()
engineMain initialStage = engineMainWith (\() -> initialStage) (pure ())

engineMainWith :: (a -> Engine.StackStage) -> Engine.StageSetupRIO a -> IO ()
engineMainWith handoff action =
  appMain getOptions optionsVerbose setup $ runStack
    [ -- XXX: run swapchain bootstrap and replace with next stage
      Bootstrap.stackStage handoff action
    ]
