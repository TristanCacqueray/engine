module Engine.Vulkan.Pipeline.Stages
  ( StageInfo(..)
  , withLabels
  ) where

import RIO

import Vulkan.Core10 qualified as Vk

class (Applicative t, Traversable t) => StageInfo t where
  stageNames :: IsString label => t label
  stageFlagBits :: t Vk.ShaderStageFlagBits

withLabels :: (StageInfo t, IsString label) => t a -> t (label, a)
withLabels staged = (,) <$> stageNames <*> staged
