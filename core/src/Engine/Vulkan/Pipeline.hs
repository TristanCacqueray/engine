module Engine.Vulkan.Pipeline
  ( Pipeline(..)
  , destroy
  , Specialization
  ) where

import RIO

import Data.Kind (Type)
import Data.Tagged (Tagged(..))
import Data.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Types (HasVulkan(..), DsLayouts)

data Pipeline (dsl :: [Type]) vertices instances = Pipeline
  { pipeline     :: Vk.Pipeline
  , pLayout      :: Tagged dsl Vk.PipelineLayout
  , pDescLayouts :: Tagged dsl DsLayouts
  }

destroy
  :: ( MonadIO io
     , HasVulkan ctx
     )
  => ctx
  -> Pipeline dsl vertices instances
  -> io ()
destroy context Pipeline{..} = do
  Vector.forM_ (unTagged pDescLayouts) \dsLayout ->
    Vk.destroyDescriptorSetLayout device dsLayout Nothing
  Vk.destroyPipeline device pipeline Nothing
  Vk.destroyPipelineLayout device (unTagged pLayout) Nothing
  where
    device = getDevice context

type family Specialization pipeline
