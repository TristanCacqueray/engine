module Engine.Stage.Bootstrap.Setup
  ( stackStage
  , bootstrapStage
  ) where

import RIO

import UnliftIO.Resource qualified as Resource

import Engine.Stage.Component qualified as Stage
import Engine.Types (StackStage(..))
import Engine.Types qualified as Engine
import Engine.StageSwitch (trySwitchStage)

stackStage
  :: (a -> StackStage)
  -> Engine.StageSetupRIO a
  -> StackStage
stackStage handoff action = StackStage $ bootstrapStage handoff action

bootstrapStage
  :: (a -> StackStage)
  -> Engine.StageSetupRIO a
  -> Engine.Stage Stage.NoRenderPass Stage.NoPipelines Stage.NoFrameResources Stage.NoRunState
bootstrapStage handoff action = Stage.assemble "Bootstrap" Stage.noRendering resources Nothing
  where
    resources = Stage.noResources
      { Stage.rInitialRS =
          transitState handoff action
      }

transitState
  :: (a -> StackStage)
  -> Engine.StageSetupRIO a
  -> Engine.StageSetupRIO (Resource.ReleaseKey, Stage.NoRunState)
transitState handoff action = do
  res <- action

  switched <- trySwitchStage . Engine.Replace $
    handoff res

  unless switched $
    logError "Bootstrap switch failed"

  key <- Resource.register $ pure ()
  pure (key, Stage.NoRunState)
