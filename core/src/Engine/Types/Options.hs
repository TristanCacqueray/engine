module Engine.Types.Options
  ( Options(..)
  , getOptions
  , optionsP
  ) where

import RIO

import Options.Applicative.Simple qualified as Opt

import Paths_keid_core qualified

-- | Command line arguments
data Options = Options
  { optionsVerbose      :: Bool
  , optionsFullscreen   :: Bool
  , optionsDisplay      :: Natural
  , optionsRecyclerWait :: Maybe Int
  }
  deriving (Show)

getOptions :: IO Options
getOptions = do
  (options, ()) <- Opt.simpleOptions
    $(Opt.simpleVersion Paths_keid_core.version)
    header
    description
    optionsP
    Opt.empty
  pure options
  where
    header =
      "Another playground"

    description =
      mempty

optionsP :: Opt.Parser Options
optionsP = do
  optionsVerbose <- Opt.switch $ mconcat
    [ Opt.long "verbose"
    , Opt.short 'v'
    , Opt.help "Show more and more detailed messages"
    ]

  optionsFullscreen <- Opt.switch $ mconcat
    [ Opt.long "fullscreen"
    , Opt.short 'f'
    , Opt.help "Run in fullscreen mode"
    ]

  optionsDisplay <- Opt.option Opt.auto $ mconcat
    [ Opt.long "display"
    , Opt.help "Select display number"
    , Opt.value 0
    ]

  optionsRecyclerWait <- Opt.optional . Opt.option Opt.auto $ mconcat
    [ Opt.long "recycler-wait"
    , Opt.help "Inject a delay before waiting for a timeline semaphore."
    ]

  pure Options{..}
