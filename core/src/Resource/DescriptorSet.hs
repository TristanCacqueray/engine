module Resource.DescriptorSet
  ( allocatePool
  , TypeMap
  , mkPoolCI
  ) where

import RIO

import UnliftIO.Resource qualified as Resource
import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk
import Vulkan.Zero (zero)

import Engine.Vulkan.Types (HasVulkan(getDevice))

allocatePool
  :: ( Resource.MonadResource m
     , MonadReader env m
     , HasVulkan env
     )
  => Word32
  -> TypeMap Word32
  -> m (Resource.ReleaseKey, Vk.DescriptorPool)
allocatePool maxSets sizes = do
  device <- asks getDevice
  Vk.withDescriptorPool device (mkPoolCI maxSets sizes) Nothing Resource.allocate

type TypeMap a = [(Vk.DescriptorType, a)]

mkPoolCI
  :: Word32
  -> TypeMap Word32
  -> Vk.DescriptorPoolCreateInfo '[]
mkPoolCI maxSets sizes = zero
  { Vk.maxSets   = maxSets
  , Vk.poolSizes = Vector.fromList $ map (uncurry Vk.DescriptorPoolSize) sizes
  }
