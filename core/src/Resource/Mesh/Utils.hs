module Resource.Mesh.Utils where

import RIO

import RIO.List.Partial (maximum)

import Geomancy (Transform, Vec4, vec4)
import Geomancy.Transform qualified as Transform

import Resource.Mesh.Types (AxisAligned(..), Measurements(..), middleAa, sizeAa)

aabbTranslate :: AxisAligned Measurements -> Transform
aabbTranslate measurements = Transform.translate aaX aaY aaZ
  where
    AxisAligned{..} = middleAa measurements

aabbScale :: AxisAligned Measurements -> Transform
aabbScale measurements = Transform.scale3 aaX aaY aaZ
  where
    AxisAligned{..} = sizeAa measurements

boundingSphere :: AxisAligned Measurements -> Vec4
boundingSphere measurements = vec4 aaX aaY aaZ radius
  where
    AxisAligned{..} = middleAa measurements
    radius = maximum (fmap mMax measurements)
