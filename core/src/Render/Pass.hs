{-# LANGUAGE OverloadedLists #-}

module Render.Pass
  ( usePass
  , setViewportScissor

  , beginInfo
  ) where

import RIO

import RIO.Vector.Partial ((!))
import Vulkan.Core10 qualified as Vk
import Vulkan.Zero (zero)

import Engine.Vulkan.Types (HasRenderPass(..))

usePass :: (MonadIO io, HasRenderPass a) => a -> Word32 -> Vk.CommandBuffer -> io r -> io r
usePass render imageIndex cb =
  Vk.cmdUseRenderPass
    cb
    (beginInfo render imageIndex)
    Vk.SUBPASS_CONTENTS_INLINE

beginInfo :: HasRenderPass a => a -> Word32 -> Vk.RenderPassBeginInfo '[]
beginInfo rp imageIndex = zero
  { Vk.renderPass  = getRenderPass rp
  , Vk.framebuffer = getFramebuffers rp ! fromIntegral imageIndex
  , Vk.renderArea  = getRenderArea rp
  , Vk.clearValues = getClearValues rp
  }

setViewportScissor
  :: ( HasRenderPass rp
     , MonadIO io
     )
  => Vk.CommandBuffer
  -> Vk.Extent2D
  -> rp
  -> io ()
setViewportScissor cb Vk.Extent2D{width, height} rp = do
  Vk.cmdSetViewport
    cb
    0
    [ Vk.Viewport
        { x        = 0
        , y        = 0
        , width    = realToFrac width
        , height   = realToFrac height
        , minDepth = 0
        , maxDepth = 1
        }
    ]
  Vk.cmdSetScissor
    cb
    0
    [ getRenderArea rp
    ]
