{-# OPTIONS_GHC -fno-prof-auto #-}

module Render.Code
  ( Code(..)
  , glsl
  , trimming
  , compileVert
  , compileFrag
  , compileComp
  ) where

import RIO

import Language.Haskell.TH (Exp, Q)
import NeatInterpolation (trimming)
import RIO.Text qualified as Text
import Vulkan.Utils.ShaderQQ.GLSL.Glslang (compileShaderQ, glsl)

-- | A wrapper to `show` code into `compileShaderQ` vars.
newtype Code = Code { unCode :: Text }
  deriving (Eq, Ord, IsString)

instance Show Code where
  show = Text.unpack . unCode

compileVert :: Code -> Q Exp
compileVert = compileShaderQ Nothing "vert" Nothing . Text.unpack . unCode

compileFrag :: Code -> Q Exp
compileFrag = compileShaderQ Nothing "frag" Nothing . Text.unpack . unCode

compileComp :: Code -> Q Exp
compileComp = compileShaderQ Nothing "comp" Nothing . Text.unpack . unCode
