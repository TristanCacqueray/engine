# Changelog for keid-sound-openal

## 0.2.0.0

- Switched to `openal-ffi`.

## 0.1.1.0

- Loader switched to `Resource.Source`.
- Removed unused deps.

## 0.1.0.0

Initial import.
