# Changelog for keid-frp-banana

## 0.1.1.0

- Added more `Engine.Window.*` wrappers to `Engine.ReactiveBanana.Window`.
- Moved `Engine.ReactiveBanana.timer` to `Engine.ReactiveBanana.Timer.every`.

## 0.1.0.0

Initial release
