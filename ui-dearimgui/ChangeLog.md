# Changelog for keid-dearimgui

## 0.1.2.0

- Added `renderWith` helper to inject DearImGui initialization in stage rendering components.

## 0.1.1.1

- Added `Vk.deviceWaitIdle` injection in `afterLoop`.
  * Should fix release/allocate race between two imgui-using stages back-to-back.

## 0.1.1.0

- Added `allocateWithFonts` from dear-imgui-1.3.0.

## 0.1.0.1

- Removed unused deps.

## 0.1.0.0

Initial import.
