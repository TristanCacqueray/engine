module Geometry.Plane
  ( Plane(..)
  , xyBack
  , xyFront
  , xzUp
  , xzDown
  , yzLeft
  , yzRight
  ) where

import RIO

import Geomancy (Vec3, vec3)

data Plane = Plane
  { normal :: Vec3
  , depth  :: Float
  }
  deriving (Show)

-- | XY plane facing back.
xyBack :: Float -> Plane
xyBack d = Plane
  { normal = vec3 0 0 (-1)
  , depth  = d
  }

-- | XY plane facing front.
xyFront :: Float -> Plane
xyFront d = Plane
  { normal = vec3 0 0 1
  , depth  = d
  }

-- | XZ plane facing up.
xzUp :: Float -> Plane
xzUp d = Plane
  { normal = vec3 0 (-1) 0
  , depth  = d
  }

-- | XZ plane facing down.
xzDown :: Float -> Plane
xzDown d = Plane
  { normal = vec3 0 1 0
  , depth  = d
  }

-- | YZ plane facing left.
yzLeft :: Float -> Plane
yzLeft d = Plane
  { normal = vec3 (-1) 0 0
  , depth  = d
  }

-- | YZ plane facing right.
yzRight :: Float -> Plane
yzRight d = Plane
  { normal = vec3 1 0 0
  , depth  = d
  }
