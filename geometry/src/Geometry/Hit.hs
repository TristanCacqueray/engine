module Geometry.Hit
  ( Hit(..)
  ) where

import RIO

import Geomancy (Vec3)

data Hit = Hit
  { distance :: Float
  , cosine   :: Float
  , position :: Vec3
  }
  deriving (Eq, Ord, Show)
