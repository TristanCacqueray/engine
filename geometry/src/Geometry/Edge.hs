module Geometry.Edge where

import RIO

data Edge a = Edge
  { edgeFrom :: a
  , edgeTo   :: a
  } deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

{-# INLINEABLE edgesR #-}
edgesR :: [a] -> Maybe [Edge a]
edgesR xs = go (Just []) xs
  where
    go acc = \case
      [] ->
        acc
      [_one] ->
        Nothing
      edgeFrom : edgeTo : next ->
        case acc of
          Nothing ->
            go (Just [Edge{..}]) next
          Just old ->
            go (Just (Edge{..} : old)) next
