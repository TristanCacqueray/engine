module Geometry.Intersect
  ( Hit
  , ray'plane
  ) where

import RIO

import Geomancy.Vec3 (dot, (^*))

import Geometry.Hit (Hit(..))
import Geometry.Plane (Plane)
import Geometry.Plane qualified as Plane
import Geometry.Ray (Ray)
import Geometry.Ray qualified as Ray

ray'plane
  :: Ray
  -> Plane
  -> Maybe Hit
ray'plane ray plane =
  -- XXX: Ignore "glancing" hits from either side of a plane
  if (abs denom > eps) then
    Just Hit
      { distance = t
      , cosine   = denom
      , position = hit
      }
  else
    Nothing
  where
    eps = 1/32

    denom =
      Plane.normal plane `dot` Ray.direction ray

    t =
      negate ((Plane.normal plane `dot` Ray.origin ray) + Plane.depth plane) / denom

    hit =
      Ray.origin ray + Ray.direction ray ^* t
