# Changelog for keid-geometry

## 0.1.1.2

- Added `Geometry.Tile.Neighbors` collection and bitset-based adjacency tests.
- Added `Geometry.Tile.Microblob` collection for sub-grid synthesis of "blob" tileset.

## 0.1.1.1

- Added ray-plane intersection and related types.
- Removed unused deps.

## 0.1.1.0

- Removed older list-based icosphere generator.

## 0.1.0.1

- Added an icosphere generator backed by a mutable vector.
