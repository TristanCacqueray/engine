module Global.Resource.CubeMap.Base.Paths where

import RIO

import Data.FileEmbed (makeRelativeToProject)
import Resource.Static as Static
import RIO.FilePath ((</>))

makeRelativeToProject ("embed" </> "cubemaps") >>=
  Static.filePatterns Static.Files
