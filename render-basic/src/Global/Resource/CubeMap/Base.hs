{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.CubeMap.Base
  ( Collection(..)
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import Resource.Source (Source)
import Resource.Source qualified as Source
import Resource.Collection (Generically1(..))

import Global.Resource.CubeMap.Base.Paths qualified as Paths

data Collection a = Collection
  { black        :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via Generically1 Collection

sources :: Collection Source
sources = Collection
  { black        = $(Source.embedFile Paths.BLACK_KTX_ZST)
  }
