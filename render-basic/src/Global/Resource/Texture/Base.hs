{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Texture.Base
  ( Collection(..)
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import Resource.Collection (Generically1(..))
import Resource.Source (Source)
import Resource.Source qualified as Source

import Global.Resource.Texture.Base.Paths qualified as Paths

data Collection a = Collection
  { black        :: a
  , flat         :: a
  , ibl_brdf_lut :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via Generically1 Collection

sources :: Collection Source
sources = Collection
  { black        = $(Source.embedFile Paths.BLACK_KTX_ZST)
  , flat         = $(Source.embedFile Paths.FLAT_KTX_ZST)
  , ibl_brdf_lut = $(Source.embedFile Paths.IBL_BRDF_LUT_KTX_ZST)
  }
