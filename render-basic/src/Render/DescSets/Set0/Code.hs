module Render.DescSets.Set0.Code
  ( set0binding0
  , set0binding1
  , set0binding2
  , set0binding3
  , set0binding4
  , set0binding5
  , set0binding5color
  , set0binding6
  ) where

import Render.Code (Code(..), trimming)

set0binding0 :: Code
set0binding0 = Code
  [trimming|
    layout(set=0, binding=0, std140) uniform Globals {
      mat4 projection;
      mat4 invProjection;
      mat4 view;
      mat4 invView;
      vec4 viewPosition;
      vec4 viewDirection;
      vec4 tweaks;
      vec4 fog;
       int envCubeId;
      uint numLights;
    } scene;
  |]

set0binding1 :: Code
set0binding1 = Code
  [trimming|
    layout(set=0, binding=1) uniform sampler samplers[];
  |]

set0binding2 :: Code
set0binding2 = Code
  [trimming|
    layout(set=0, binding=2) uniform texture2D textures[];
  |]

set0binding3 :: Code
set0binding3 = Code
  [trimming|
    layout(set=0, binding=3) uniform textureCube cubes[];
  |]

set0binding4 :: Code
set0binding4 = Code
  [trimming|
    layout(set=0, binding=4, std140) uniform Lights {
      Light lights[MAX_LIGHTS];
    };
  |]

set0binding5 :: Code
set0binding5 = Code
  [trimming|
    layout(set=0, binding=5) uniform sampler2DArrayShadow shadowmaps;
  |]

-- XXX: for shadowmap mode in Debug pipeline
set0binding5color :: Code
set0binding5color = Code
  [trimming|
    layout(set=0, binding=5) uniform sampler2DArray shadowmaps;
  |]

set0binding6 :: Code
set0binding6 = Code
  [trimming|
    layout(set=0, binding=6, std140) uniform Materials {
      Material materials[MAX_MATERIALS];
    };
  |]
