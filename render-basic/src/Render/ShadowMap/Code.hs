module Render.ShadowMap.Code
  ( vert
  ) where

import RIO

import Render.Code (Code, glsl)
import Render.Code.Lit (structLight)
import Render.DescSets.Sun (pattern MAX_VIEWS)

vert :: Code
vert = fromString
  [glsl|
    #version 450
    #extension GL_EXT_multiview : enable

    ${structLight}

    layout(set=0, binding=0, std140) uniform Globals {
      Light lights[${MAX_VIEWS}];
    };

    layout(location = 0) in vec3 vPosition;

    layout(location = 1) in mat4 iModel;

    void main() {
      gl_Position
        = lights[gl_ViewIndex].viewProjection
        * iModel
        * vec4(vPosition, 1.0);
    }
  |]
