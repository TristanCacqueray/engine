{-# LANGUAGE OverloadedLists #-}

module Render.Skybox.Pipeline
  ( Config
  , config
  , Pipeline
  , allocate

  , stageCode
  , stageSpirv
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (HasVulkan, HasRenderPass(..), DsBindings)
import Render.Code (compileVert, compileFrag)
import Render.DescSets.Set0 (Scene)
import Render.Skybox.Code qualified as Code

type Pipeline = Graphics.Pipeline '[Scene] () ()
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate
  :: ( HasVulkan env
     , HasRenderPass renderpass
     )
  => Vk.SampleCountFlagBits
  -> Tagged Scene DsBindings
  -> renderpass
  -> ResourceT (RIO env) Pipeline
allocate multisample tset0 = do
  fmap snd . Graphics.allocate
    Nothing
    multisample
    (config tset0)

config :: Tagged Scene DsBindings -> Config
config (Tagged set0) = Graphics.baseConfig
  { Graphics.cStages      = stageSpirv
  , Graphics.cDescLayouts = Tagged @'[Scene] [set0]
  , Graphics.cCull        = Vk.CULL_MODE_NONE
  }

stageCode :: Graphics.StageCode
stageCode = Graphics.basicStages Code.vert Code.frag

stageSpirv :: Graphics.StageSpirv
stageSpirv = Graphics.basicStages vertSpirv fragSpirv

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
