module Render.Unlit.Colored.Model
  ( Model

  , VertexAttrs
  , vkVertexAttrs

  , rgbF
  , black
  , white

  , InstanceAttrs
  ) where

import RIO

import Geomancy (Transform, Vec4, vec4)
import Geomancy.Vec3 qualified as Vec3
import Resource.Model qualified as Model
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs

type VertexAttrs = "RGBA" ::: Vec4

type InstanceAttrs = Transform

vkVertexAttrs :: [Vk.Format]
vkVertexAttrs =
  [ Vk.FORMAT_R32G32B32A32_SFLOAT -- vColor :: vec4
  ]

{-# INLINE rgbF #-}
rgbF :: Float -> Float -> Float -> VertexAttrs
rgbF r g b = vec4 r g b 1

{-# INLINE black #-}
black :: VertexAttrs
black = rgbF 0 0 0

{-# INLINE white #-}
white :: VertexAttrs
white = rgbF 1 1 1
