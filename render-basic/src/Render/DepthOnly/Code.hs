module Render.DepthOnly.Code
  ( vert
  ) where

import RIO

import Render.Code (Code, glsl)
import Render.DescSets.Set0.Code (set0binding0)

vert :: Code
vert = fromString
  [glsl|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    invariant gl_Position;

    ${set0binding0}

    layout(location = 0) in vec3 vPosition;
    layout(location = 1) in mat4 iModel;

    // layout(location = 0) out vec4 fColor;

    void main() {
      vec4 fPosition = iModel * vec4(vPosition, 1.0);

      gl_Position
        = scene.projection
        * scene.view
        * fPosition;

      // XXX: needed for alpha cut-outs
      // fColor = vColor;
      // fColor.rgb * fColor.a;
    }
  |]
