{-# OPTIONS_GHC -fplugin Foreign.Storable.Generic.Plugin #-}

module Render.Lit.Material
  ( Material(..)
  , shiftTextures
  ) where

import RIO

import Foreign.Storable.Generic (GStorable)
import Geomancy (Vec2, Vec4, vec2, vec4)
import Vulkan.Zero (Zero(..))

data Material = Material
  { mBaseColor         :: Vec4
  , mMetallicRoughness :: Vec2
  , mEmissive          :: Vec4
  , mNormalScale       :: Float
  , mAlphaCutoff       :: Float

  , mBaseColorTex         :: Int32
  , mMetallicRoughnessTex :: Int32
  , mEmissiveTex          :: Int32
  , mNormalTex            :: Int32
  , mAmbientOcclusionTex  :: Int32
  }
  deriving (Eq, Ord, Show, Generic)

instance GStorable Material

instance Zero Material where
  zero = Material
    { mBaseColor         = vec4 1.0 1.0 1.0 1.0
    , mMetallicRoughness = vec2 0 0.6
    , mEmissive          = vec4 0 0 0 0
    , mNormalScale       = 1.0
    , mAlphaCutoff       = 0.5

    , mBaseColorTex         = -1
    , mMetallicRoughnessTex = -1
    , mNormalTex            = -1
    , mEmissiveTex          = -1
    , mAmbientOcclusionTex  = -1
    }

shiftTextures :: Int32 -> Material -> Material
shiftTextures offset Material{..} = Material
  { mBaseColorTex =
      if mBaseColorTex > -1 then
        mBaseColorTex + offset
      else
        mBaseColorTex
  , mMetallicRoughnessTex =
      if mMetallicRoughnessTex > -1 then
        mMetallicRoughnessTex + offset
      else
        mMetallicRoughnessTex
  , mEmissiveTex =
      if mEmissiveTex > -1 then
        mEmissiveTex + offset
      else
        mEmissiveTex
  , mNormalTex =
      if mNormalTex > -1 then
        mNormalTex + offset
      else
        mNormalTex
  , mAmbientOcclusionTex =
      if mAmbientOcclusionTex > -1 then
        mAmbientOcclusionTex + offset
      else
        mAmbientOcclusionTex
  , ..
  }
