module Render.Lit.Textured.Code
  ( vert
  , frag
  ) where

import RIO

import Render.Code (Code, glsl)
import Render.Code.Lit (litMain, shadowFuns, structLight, brdfSpecular)
import Render.DescSets.Set0.Code (set0binding0, set0binding1, set0binding2, set0binding3, set0binding4, set0binding5)

vert :: Code
vert = fromString
  [glsl|
    #version 450

    invariant gl_Position;

    ${set0binding0}

    // vertexPos
    layout(location = 0) in vec3 vPosition;
    // vertexAttrs
    layout(location = 1) in vec2 vTexCoord;
    layout(location = 2) in vec3 vNormal;
    // textureParams
    layout(location = 3) in  vec4 iTextureScaleOffset;
    layout(location = 4) in  vec4 iTextureGamma;
    layout(location = 5) in ivec2 iTextureIds;

    // transformMat
    layout(location = 6) in mat4 iModel;

    layout(location = 0)      out  vec4 fPosition;
    layout(location = 1)      out  vec3 fNormal;
    layout(location = 2)      out  vec2 fTexCoord;
    layout(location = 3) flat out  vec4 fTextureGamma;
    layout(location = 4) flat out ivec2 fTextureIds;

    void main() {
      fPosition = iModel * vec4(vPosition, 1.0);

      gl_Position
        = scene.projection
        * scene.view
        * fPosition;

      fNormal = transpose(mat3(inverse(iModel))) * vNormal; // TODO: use modelInv

      fTexCoord     = vTexCoord * iTextureScaleOffset.st + iTextureScaleOffset.pq;
      fTextureGamma = iTextureGamma;
      fTextureIds   = iTextureIds;
    }
  |]

frag :: Code
frag = fromString
  [glsl|
    #version 450
    #extension GL_EXT_nonuniform_qualifier : enable

    layout(early_fragment_tests) in;

    // XXX: copypasta from Lit.Colored
    // TODO: move to spec constant
    const uint MAX_LIGHTS = 255;
    const float PCF_STEP = 1.5 / 4096;

    // TODO: move to material
    const float reflectivity = 1.0/256.0;

    ${structLight}

    ${set0binding0}
    ${set0binding1}
    ${set0binding2}
    ${set0binding3}
    ${set0binding4}
    ${set0binding5}

    layout(location = 0)      in  vec4 fPosition;
    layout(location = 1)      in  vec3 fNormal;
    layout(location = 2)      in  vec2 fTexCoord;
    layout(location = 3) flat in  vec4 fTextureGamma;
    layout(location = 4) flat in ivec2 fTextureIds;

    layout(location = 0) out vec4 oColor;

    ${shadowFuns}
    ${brdfSpecular}

    void main() {
      // XXX: fall back to solid color
      vec4 baseColor = fTextureGamma;
      if ((fTextureIds.t > -1) && (fTextureIds.s > -1)) {
        vec4 texel = texture(
          sampler2D(
            textures[nonuniformEXT(fTextureIds.t)],
            samplers[nonuniformEXT(fTextureIds.s)]
          ),
          fTexCoord
        );

        if (texel.a < 0.5) discard;

        vec3 color = pow(texel.rgb, fTextureGamma.rgb);
        float combinedAlpha = texel.a * fTextureGamma.a;
        baseColor = vec4(color * combinedAlpha, combinedAlpha);
      }

      // TODO: get from textures
      float metallic = 0.0;
      float roughness = 0.6;
      float nonOcclusion = 1.0;

      vec3 normal = normalize(fNormal);

      ${litMain}
    }
  |]
