module Render.Lit.Colored.Model
  ( Model

  , VertexAttrs(..)
  , vkVertexAttrs

  , InstanceAttrs
  ) where

import RIO

import Foreign (Storable(..))
import Geomancy (Transform, Vec2, Vec4)
import Geomancy.Vec3 qualified as Vec3
import Resource.Model qualified as Model
import Vulkan.Core10 qualified as Vk

type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs

data VertexAttrs = VertexAttrs
  { vaBaseColor         :: Vec4
  , vaEmissiveColor     :: Vec4 -- XXX: a: alpha cutoff
  , vaMetallicRoughness :: Vec2
  , vaNormal            :: Vec3.Packed
  }
  deriving (Eq, Ord, Show, Generic)

instance Storable VertexAttrs where
  alignment ~_ = 4

  sizeOf ~_ = 16 + 16 + 8 + 12

  peek ptr = do
    vaBaseColor         <- peekByteOff ptr  0
    vaEmissiveColor     <- peekByteOff ptr 16
    vaMetallicRoughness <- peekByteOff ptr 32
    vaNormal            <- peekByteOff ptr 40
    pure VertexAttrs{..}

  poke ptr VertexAttrs{..} = do
    pokeByteOff ptr  0 vaBaseColor
    pokeByteOff ptr 16 vaEmissiveColor
    pokeByteOff ptr 32 vaMetallicRoughness
    pokeByteOff ptr 40 vaNormal

type InstanceAttrs = Transform

vkVertexAttrs :: [Vk.Format]
vkVertexAttrs =
  [ Vk.FORMAT_R32G32B32A32_SFLOAT -- vBaseColor         :: vec4
  , Vk.FORMAT_R32G32B32A32_SFLOAT -- vEmissiveColor     :: vec4
  , Vk.FORMAT_R32G32_SFLOAT       -- vMetallicRoughness :: vec2
  , Vk.FORMAT_R32G32B32_SFLOAT    -- vNormal            :: vec3
  ]
