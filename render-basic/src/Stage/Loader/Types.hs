module Stage.Loader.Types
  ( Stage
  , Frame

  , FrameResources(..)
  , RunState(..)
  ) where

import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0

import Stage.Loader.UI (UI)
import Stage.Loader.UI qualified as UI

type Stage = Basic.Stage FrameResources RunState

type Frame = Basic.Frame FrameResources

data FrameResources = FrameResources
  { frSceneUi :: Set0.FrameResource '[Set0.Scene]

  , frUI :: UI.Observer
  }

data RunState = RunState
  { rsSceneUiP    :: Set0.Process

  , rsUI :: UI
  }
