module Stage.Loader.Scene
  ( Process
  , spawn
  ) where

import RIO

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..), emptyScene)

type Process = Worker.Merge Scene

spawn
  :: ( MonadUnliftIO m
     , Worker.HasOutput projection
     , Worker.GetOutput projection ~ Camera.Projection 'Camera.Orthographic
     )
  => projection
  -> m Process
spawn = Worker.spawnMerge1 mkScene

mkScene :: Camera.Projection 'Camera.Orthographic -> Scene
mkScene Camera.Projection{..} =
  emptyScene
    { sceneProjection    = projectionTransform
    , sceneInvProjection = projectionInverse
    }
