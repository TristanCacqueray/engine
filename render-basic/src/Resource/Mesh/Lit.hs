{-# OPTIONS_GHC -fplugin Foreign.Storable.Generic.Plugin #-}

module Resource.Mesh.Lit
  ( MaterialNodes
  , MaterialNode(..)
  ) where

import RIO

import Foreign.Storable.Generic (GStorable)
import RIO.Vector.Storable qualified as Storable

import Render.Lit.Material (Material)
import Resource.Mesh.Types (HasRange(..), Node)

type MaterialNodes = Storable.Vector MaterialNode

data MaterialNode = MaterialNode
  { mnNode       :: Node
  , mnMaterialIx :: Int
  , mnMaterial   :: Material
  }
  deriving (Eq, Show, Generic)

instance GStorable MaterialNode

instance HasRange MaterialNode where
  {-# INLINE getRange #-}
  getRange = getRange . mnNode

  {-# INLINE adjustRange #-}
  adjustRange mn@MaterialNode{mnNode} newFirstIndex = mn
    { mnNode = adjustRange mnNode newFirstIndex
    }
