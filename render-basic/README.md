# Keid Engine - Basic forward rendering

- Forward renderpass with MSAA
  * Common descriptor set
  * Unlit pipelines
    * Colored
    * Textured
    * Tile map
    * Wires
  * Lit pipelines
    * Colored
    * Textured
    * Material
  * Skybox pipeline
  * Debug pipelines
    * UV
    * Shadowmap
- Shadowmap pass
  * Shadow-casting pipeline
- Depth-only
- Asset loader stage
