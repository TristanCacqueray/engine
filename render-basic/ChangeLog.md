# Changelog for keid-render-basic

## 0.1.7.0

- Pipelines updated to use `Engine.Vulkan.Pipeline.Graphics`.
- Pipelines wrapped in a HKD.
  * The `Pipelines`  is now an alias for `Identity` wrapper.
  * The data type is now `PipelinesF f` with a hole for HKD parameter.
  * The constructor name is still `Pipelines`, with all the fields intact.
- Added a bunch of functions to work with runtime-reloadable pipelines.
- Expose `stageCode` and `stageSpirv` for all pipelines.
- Added `Render.Basic.rendering` reusable stage component.

## 0.1.6.1

- `withFrozenCallStack` fixes for GHC-9.0.
- Added color helpers to `Render.Unlit.Colored`.
- Added `Render.Unlit.Sprite` pipeline.
  * Available as `pSprite` and `pSpriteOutline`.

## 0.1.6.0

- Updated `Loader` stage to camera changes in core.
- Import sun (directional light) functions from demos.

## 0.1.5.0

- Extracted deprecated `allocateRenderpass_` into `Render.Basic.allocate`.
- Extracted hardcoded shadowmap params into `Render.Basic.Settings`.
- Added `Render.Unlit.TileMap` pipeline.
- Added `Render.DepthOnly` pipeline.
- Updated lit pipelines to match vertex position calculation.
- Fixed shadowmap index guard.
- Pipelines updated for new configs with specialization.
  * `pDebug` is split into multiple pipelines, specialized by mode.

## 0.1.4.0

Update to match `keid-core-0.1.4.0`.

- Changed `allocatePipelines` to use `msaa` directly.
  (`allocatePipelines_` remains unchanged.)
- Fixed litMain check for missing shadowmap.
- Relaxed `Set0.allocate`, `Set0.allocateEmpty` to any `MonadVulkan` under `ResourceT`.
- Added pipeline config exports.
- Added shared pipeline params to collection.

## 0.1.3.0

- Replaced RIO in resource loaders with constraints.
- `ForwardMSAA` and `ShadowMap` updated to `keid-core-0.1.3.0`.

## 0.1.2.1

- Fixed missing embedded textures in source distribution.

## 0.1.2.0

- Added embedded base textures and cubes needed for shader code.
- Changed Font resource to Source loader for both container and texture.
- Added callstack freezing to report call sites in debug log.
- Changed Loader stage to use `Source` in bootstrap.
- Removed `Set0.set0` and `Set0.set0_`.
  - Added `Set0.mkBindings` instead.
  - Minimal descriptor counts derived from base collections to prevent HOOM errors.

## 0.1.1.4

- Fixed another VK_HOST_OUT_OF_MEMORY, in Loader stage.

## 0.1.1.3

- Removed unused deps.
- Fixed VK_HOST_OUT_OF_MEMORY due to zero cubemap descriptors provided in Basic pipelines.

## 0.1.1.2

- Fix ambient occlusion in LitMaterial shader setup.
- Fix roughness clamping in Lit shader.

## 0.1.1.1

- Update timer to match `keid-core`.

## 0.1.1.0

- Added `Stage.Loader.*` (and `keid-geometry` dependency with it).

## 0.1.0.0

- Initial public version.
